#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 14:18:30 2016

@author: lucadelu
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import sys
import logging
from .utils import urljoin, create_region_folder

PYVERSION = sys.version_info
if PYVERSION.major == 3:
    import urllib.request as urllib
    import configparser
elif PYVERSION.major == 2:
    import urllib2 as urllib
    import ConfigParser as configparser


class DownloadData:
    """Class to download

    :param str confile: the path to configuration file
    :param str out_dir: the directory where to save the downloaded files
    :param bool debug: True to get more info in the debug log file
    """
    def __init__(self, confile, out_dir=None, debug=False):
        # configuration file
        self.config = configparser.ConfigParser()
        self.config.read(confile)
        # useful info
        self.name = self.config.get('general', 'name').lower()
        self.regions = self.config.get('regions', 'list').split()
        # set destination folder
        if not out_dir:
            if self.config.get('general', 'out_dir'):
                out_dir = self.config.get('general', 'out_dir')
            else:
                raise Exception("Folder to store downloaded files has "
                                "to be set during initialization or in "
                                "the configure file into 'general' "
                                "section, 'out_dir' parameter")
        if os.access(out_dir, os.W_OK):
            self.out_dir = out_dir
        else:
            try:
                os.mkdir(out_dir)
                self.out_dir = out_dir
            except:
                raise Exception("Folder to store downloaded files does"
                                " not exist or is not writeable")
        self.ftp = False
        # debug
        self.debug = debug
        # for logging
        log_filename = os.path.join(self.out_dir,
                                    '{pro}.log'.format(pro=self.name))
        log_format = '%(asctime)s - %(levelname)s - %(message)s'
        logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                            format=log_format)
        # get regions with url
        self.down_regions = []
        self._regions_with_url()

    def _regions_with_url(self):
        """Private function to get regions with url"""
        for r in self.regions:
            if self.config.get(r, 'url'):
                self.down_regions.append(r)
        if self.debug:
            logging.debug("regions with url {li}".format(li=' - '.join(self.down_regions)))

    def _test_url(self, url):
        """Private function to test if a url exists

        :param str url: a url to test
        """
        r = urllib.Request(url)
        r.get_method = lambda: 'HEAD'
        try:
            urllib.urlopen(r)
            return True
        except Exception:
            return False

    def test_urls(self, output=None):
        """Test all the urls if they exists or not

        :param str output: the path to the output file, if None print to stdout
        """
        if output:
            out = open(output, 'w')
        else:
            out = sys.stdout
        # for each region check if the url exists
        for reg in self.down_regions:
            if self.config.get(reg, 'local') == 'true':
                continue
            if self.config.get(reg, 'zip') == 'true':
                zipp = '.zip'
            url = self.config.get(reg, 'url')
            if self.config.get(reg, 'collect'):
                coll = self.config.get(reg, 'collect').split('-')
                interval = int(self.config.get(reg, 'collect_int'))
                test = False
                count = 0
                for n in range(int(coll[0]), int(coll[1]), interval):
                    url_col = urljoin(url, "{fi}{su}".format(fi=n, su=zipp))
                    if self._test_url(url_col):
                        test = True
                        count += 1
                    else:
                        name = "{fi}{su}".format(fi=str(n).zfill(len(coll[1])),
                                                 su=zipp)
                        url_col = urljoin(url, name)
                        # check with leading zeros
                        if self._test_url(url_col):
                            test = True
                            count += 1
                out.write("{reg}: {res} - {cou}\n".format(reg=reg, res=test,
                                                          cou=count))
            else:
                test = self._test_url(url)
                out.write("{reg}: {res}\n".format(reg=reg, res=test))

    def _download_file(self, url, outfile):
        """Private function to download singolar data

        :param str url: the url to download data
        :param str outfile: the path to the output file
        """
        # open output file
        filSave = open(outfile, "wb")
        # download the data
        http = urllib.urlopen(url)
        if not self.ftp:
            orig_size = http.headers['content-length']
        filSave.write(http.read())
        filSave.close()
        # if the url is ftp the function stop here, not other controls are done
        if self.ftp:
            logging.debug("{ur} downloaded".format(ur=url))
            return True
        # check that original and downloaded data have the same size
        transf_size = os.path.getsize(filSave.name)
        if int(orig_size) != int(transf_size):
            if self.debug:
                logging.debug("{ur} different size: web {we}, local "
                              "{lo}".format(ur=url, we=orig_size,
                                            lo=transf_size))
            os.remove(filSave.name)
            self._download_file(url, outfile)
        else:
            if self.debug:
                logging.debug("{ur} downloaded".format(ur=url))
        return True

    def download_shapefile(self):
        """Download the shapefile of boundaries"""
        if self.config.get('general', 'shapefile_zip') == 'true':
            name = "{inp}.zip".format(inp=self.name)
        else:
            name = "{inp}.{fo}".format(inp=self.name,
                                       fo=self.config.get('general',
                                                          'shapefile_format'))
        outfile = os.path.join(self.out_dir, name)
        if self.config.get('general', 'shapefile_url'):
            self._download_file(self.config.get('general', 'shapefile_url'),
                                outfile)

    def download_single_region(self, region):
        """Private function to download data for a singolar region

        :param str region: the name of region to download
        """
        url = self.config.get(region, 'url')
        if self.config.get(region, 'local') == 'true':
            dirr = os.path.join(self.out_dir, region)
            fo = self.config.get(region, 'format')
            os.mkdir(dirr)
            os.symlink(url, os.path.join(dirr,
                                         "{su}.{ex}".format(su=region,
                                                            ex=fo)))
            return True
        # check if it is ftp or http
        if self.config.get(region, 'ftp') == 'true':
            self.ftp = True
        else:
            self.ftp = False
        zipp = ''
        # try to create the folder where save the data
        try:
            create_region_folder(self.out_dir, region)
        except Exception:
            return False
        # get if it is a zip file
        if self.config.get(region, 'zip') == 'true':
            zipp = '.zip'
            suff = zipp
        else:
            suff = '.' + fo

        if self.debug:
            logging.debug("############# {li} #############".format(li=region))
        # if the dtm is divided in several tiles it download it
        if self.config.get(region, 'collect'):
            if self.debug:
                logging.debug("Several data to download")
            # get the possible range of data
            coll = self.config.get(region, 'collect').split('-')
            interval = int(self.config.get(region, 'collect_int'))
            # for each possible tile
            for n in range(int(coll[0]), int(coll[1]), interval):
                url_col = urljoin(url, "{fi}{su}".format(fi=n, su=zipp))
                # check if exists without leading zero
                if self._test_url(url_col):
                    if self.debug:
                        logging.debug("{ur} exists".format(ur=url_col))
                    outfile = os.path.join(self.out_dir, region,
                                           "{fi}{su}".format(fi=n, su=suff))
                    self._download_file(url_col, outfile)
                else:
                    name = "{fi}{su}".format(fi=str(n).zfill(len(coll[1])),
                                             su=zipp)
                    url_col = urljoin(url, name)
                    # check with leading zeros
                    if self._test_url(url_col):
                        if self.debug:
                            logging.debug("{ur} exists".format(ur=url_col))
                        outfile = os.path.join(self.out_dir, region,
                                               "{fi}{su}".format(fi=n,
                                                                 su=suff))
                        self._download_file(url_col, outfile)
                    # the url doesn't exist
                    else:
                        if self.debug:
                            logging.debug("{ur} doesn't exists".format(ur=url_col))
        # download a single file
        else:
            outfile = os.path.join(self.out_dir, region,
                                   "{fi}{su}".format(fi=region, su=suff))
            self._download_file(url, outfile)
        return True

    def all_regions(self):
        """Download data for all region"""
        for r in self.down_regions:
            self.download_single_region(r)
