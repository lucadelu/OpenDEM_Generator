#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 10:21:19 2016

@author: lucadelu

"""
import os
import zipfile
try:
    import osgeo.gdal as gdal
except ImportError:
    try:
        import gdal
    except ImportError:
        raise ImportError('Python GDAL library not found, please install '
                          'python-gdal')


class GdalFileInfo:

    """A class holding information about a GDAL file.

       Class copied from gdal_merge.py

       :param str filename: Name of file to read.

       :return: 1 on success or 0 if the file can't be opened.

    """

    def init_from_name(self, filename):
        """Initialize file_info from filename."""
        fh = gdal.Open(filename)
        if fh is None:
            return 0

        self.filename = filename
        self.bands = fh.RasterCount
        self.xsize = fh.RasterXSize
        self.ysize = fh.RasterYSize
        self.band = fh.GetRasterBand(1)
        self.band_type = self.band.DataType
        self.block_size = self.band.GetBlockSize()
        self.projection = fh.GetProjection()
        self.geotransform = fh.GetGeoTransform()
        self.ulx = self.geotransform[0]
        self.uly = self.geotransform[3]
        self.lrx = self.ulx + self.geotransform[1] * self.xsize
        self.lry = self.uly + self.geotransform[5] * self.ysize

        meta = fh.GetMetadata()
        if '_FillValue' in meta.keys():
            self.fill_value = meta['_FillValue']
        elif self.band.GetNoDataValue():
            self.fill_value = self.band.GetNoDataValue()
        else:
            self.fill_value = None

        ct = fh.GetRasterBand(1).GetRasterColorTable()
        if ct is not None:
            self.ct = ct.Clone()
        else:
            self.ct = None
        return 1


def readfile(path):
    """Read the file and return a string

    :param str path: the path to a file

    """
    f = open(path, 'r')
    s = f.read()
    f.close()
    return s


def writefile(path, s):
    """Write the file from a string, new line should be added in the string

    :param str path: the path to a file
    :param str s: the string to write into file

    """
    f = open(path, 'w')
    f.write(s)
    f.close()


def urljoin(*args):
    """Joins given arguments into a url.
    Trailing but not leading slashes are stripped for each argument.
    http://stackoverflow.com/a/11326230

    :return: a string

    """

    return "/".join(map(lambda x: str(x).rstrip('/'), args))


def create_region_folder(outdir, region):
    """If it doesn't exist create a folder where save downloaded data

    :param str region: the name of region to download

    """
    reg_dir = os.path.join(outdir, region)
    if not os.path.exists(reg_dir):
        os.mkdir(reg_dir)
    else:
        if not os.access(reg_dir, os.W_OK):
            raise Exception("Folder to store regional data for {reg} "
                            "files exists but is not "


                            "writeable".format(reg=region))


def unzip(name, dirr, ext,  log):
    """Function to unzip file for a region

    :param str name: input zipfile name
    :param str dirr: the name of input/output directory
    :param str ext: extention of geodata to extract

    :param obj log: a logging object

    :return: return the number of files

    """
    count = 0
    try:
        with zipfile.ZipFile(name) as myzip:
            zlist = myzip.filelist
            for fil in zlist:
                myzip.extract(fil, dirr)
                if fil.filename.endswith(ext):
                    count += 1
    except Exception:
        log.debug("{ur} seems to not be a zip file".format(ur=name))
    return count
