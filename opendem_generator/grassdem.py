#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 15:50:38 2016

@author: lucadelu

"""

import sys
import os
import subprocess
PIPE = subprocess.PIPE
try:
    import osgeo.osr as osr
except ImportError:
    try:
        import osr
    except ImportError:
        raise ImportError('Python GDAL library not found, please install '
                          'python-gdal')
from .utils import readfile, writefile
import shutil


class GrassProcess:
    """Class to run GRASS command

    :param str confile: the path to the configuration file
    """

    def __init__(self, confile):
        """Function to initialize GrassProcess class"""
        # useful parameters for GRASS
        self.config = confile
        self.grassbin = self.config.get('grass', 'binary')
        self.grassdata = self.config.get('grass', 'database')
        self.location = self.config.get('grass', 'location')
        self.epsg = int(self.config.get('general', 'epsg'))
        self.projection = osr.SpatialReference()
        self.projection.ImportFromEPSG(self.epsg)
        self.resolution = self.config.get('general', 'resolution')
        self.centername = 'center_point'
        # initialize empty variables
        self.mapset = None
        self.mapsetpath = None

    def initialize_grass(self, overwrite=True,  mapset=None):
        """This function is needed to set up a GRASS GIS session without
        start up GRASS

        :param bool overwrite: if True set overwrite for all GRASS command
        :param str mapset: the name of an existing mapset to use
        """

        startcmd = [self.grassbin, '--config', 'path']
        p = subprocess.Popen(startcmd, shell=False, stdin=PIPE,
                             stdout=PIPE, stderr=PIPE)
        out, err = p.communicate()

        if p.returncode != 0:
            raise Exception("Error starting GRASS: probably is missing a "
                            "parameter all the keys in the 'grass' section of"
                            " the configuration file are required")
        if sys.platform.startswith('linux'):
            gisbase = out.strip()
        elif sys.platform.startswith('win'):
            if out.find("OSGEO4W home is") != -1:
                gisbase = out.strip().splitlines()[1]
            else:
                gisbase = out.strip()
            os.environ['GRASS_SH'] = os.path.join(gisbase, 'msys', 'bin',
                                                  'sh.exe')
        # Set GISBASE environment variable
        os.environ['GISBASE'] = gisbase
        # define GRASS-Python environment
        gpydir = os.path.join(gisbase, "etc", "python")
        sys.path.append(gpydir)

        # DATA
        # define GRASS DATABASE
        # Set GISDBASE environment variable
        os.environ['GISDBASE'] = self.grassdata
        os.environ['PATH'] += os.pathsep + os.path.join(gisbase, 'extrabin')

        self.mapset = 'opendem_{pid}'.format(pid=os.getpid())
        # create the location if doesn't exists
        locexist = os.path.join(self.grassdata, self.location)
        if not os.path.exists(locexist):
            if not self.epsg:
                raise Exception("Error running GRASS: EPSG code is missing")
            startcmd = self.grassbin + ' -c EPSG:' + str(self.epsg) + ' -e ' + locexist
            p = subprocess.Popen(startcmd, shell=True, stdin=PIPE,
                                 stdout=PIPE, stderr=PIPE)
            out, err = p.communicate()
            if p.returncode != 0:
                raise Exception("Error running GRASS: failed to create new "
                                "location")
        # create the mapset if doesn't exists
        self.mapsetpath = os.path.join(self.grassdata, self.location,
                                       self.mapset)
        if not os.path.exists(self.mapsetpath):
            os.mkdir(self.mapsetpath)
            wind = readfile(os.path.join(self.grassdata, self.location,
                                         "PERMANENT", "DEFAULT_WIND"))
            writefile(os.path.join(self.mapsetpath, "WIND"), wind)

        # set the environment variables
        path = os.getenv('LD_LIBRARY_PATH')
        dirlib = os.path.join(gisbase, 'lib')
        if path:
            path = dirlib + os.pathsep + path
        else:
            path = dirlib
        os.environ['LD_LIBRARY_PATH'] = path

        # language
        os.environ['LANG'] = 'en_US'
        os.environ['LOCALE'] = 'C'
        ###########
        # launch session
        import grass.script.setup as gsetup
        gsetup.init(gisbase, self.grassdata, self.location, self.mapset)
        if 'GRASS_PROJSHARE' not in os.environ.keys():
            os.environ['GRASS_PROJSHARE'] = 'C:\OSGeo4W\share\proj'

        if 'GRASS_PYTHON' not in os.environ.keys():
            os.environ['GRASS_PYTHON'] = 'C:\OSGeo4W\bin\python.exe'

        if 'SHELL' not in os.environ.keys():
            os.environ['SHELL'] = 'C:\Windows\system32\cmd.exe'
        import grass.script.core as gcore
        gcore.os.environ['GRASS_OVERWRITE'] = str(int(overwrite))

    def _run_command(self, comm, ret=False):
        """Execute a GRASS GIS command

        :param list comm: a list with all the option to run a GRASS GIS command
        :param bool ret: True to return the output of command
        """

        import grass.script.core as gcore

        runcom = gcore.Popen(comm, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, err = runcom.communicate()
        # check if an error occurred
        if runcom.returncode != 0:
            raise Exception("Error running GRASS command {com} "
                            "{e}".format(com=comm[0], e=err))
        # return standard output
        if ret:
            return out
        else:
            return 0

    def _link_grass(self, inp, intemp):
        """Link raster data into GRASS database, this function is used when
        input data has the same projection system of output data

        :param str inp: the path to source data
        :param str intemp: the name inside GRASS database
        """

        cmd = ['r.external', 'input={i}'.format(i=inp),
               'output={o}'.format(o=intemp)]
        self._run_command(cmd)

    def _import_grass(self, inp, intemp):
        """Import raster data into GRASS database, this function is used when
        input data has the different projection system of output data

        :param str inp: the path to source data
        :param str intemp: the name inside GRASS database
        """

        cmd = ['r.import', 'input={i}'.format(i=inp),
               'output={o}'.format(o=intemp)]
        self._run_command(cmd)

    def add_to_grass(self, inp, intemp, epsg):
        """Add data into GRASS database

        :param str inp: the path to source data
        :param str intemp: the name inside GRASS database
        :param str epsg: the epsg code of input data
        """

        # if the projection system is the same of the output one just link it
        if epsg == self.epsg:
            self._link_grass(inp, intemp)
        # otherwise import it
        else:
            self._import_grass(inp, intemp)

    def import_vector(self, inp, intemp):
        """Import vector data into GRASS database, this function is used when
        input data has the different projection system of output data

        :param str inp: the path to source data
        :param str intemp: the name inside GRASS database
        """

        cmd = ['v.import', 'input={i}'.format(i=inp),
               'output={o}'.format(o=intemp)]
        self._run_command(cmd)

    def exists(self, name, vector=False):
        """Check if a map exists or not a map

        :param str inp: the name of input map
        :param bool vector: True in input is vector data, otherwise raster


        :return: True or False
        """
        cmd = ['g.list', 'pattern={na}'.format(na=name)]
        if vector:
            cmd.append('type=vector')
        else:
            cmd.append('type=raster')
        out = self._run_command(cmd, ret=True)
        if out != 0 and out.strip() == name:
            return True
        else:
            return False

    def info(self, inp, vector=False):
        """Return informations about a map

        :param str inp: the name of input map

        :param bool vector: True in input is vector data, otherwise raster
        """
        # TODO fix with parse_command, see how it works on windows
        return 1
        cmd = []
        if vector:
            cmd.append('v.info')
        else:
            cmd.append('r.info')
        cmd.extend(['map={ip}'.format(ip=inp), '-g'])

    def overlay(self, ainp, binp, out, ope):
        """Perform overlay between two inputs using the operator

        :param str ainp: the name of the first input map
        :param str binp: the name of the second input map
        :param str out: the name of outut map

        :param str ope: the value of operator to use
        """
        cmd = ['v.overlay', 'ainput={inp}'.format(inp=ainp),
               'binput={inp}'.format(inp=binp), 'output={ou}'.format(ou=out),
               'operator={op}'.format(op=ope)]
        self._run_command(cmd)

    def set_region(self, inp=None, res=None, align=True):
        """Set the region according raster and resolution

        :param str inp: the name of input raster map to use as mask
        :param int res: the resolution to use

        :param bool align: True to set '-a' flag in g.region
        """
        if not inp and not res:
            raise Exception("For region function you have to set or 'inp' "
                            " or 'res' parameter")
        cmd = ['g.region']
        if align:
            cmd.append('-a')
        if inp:
            cmd.append('raster={inp}'.format(inp=inp))
        if res:
            cmd.append('res={st}'.format(st=res))
        self._run_command(cmd)

    def get_region(self, flags='-g'):
        """Return region information according the given flags

        :param str flags: a string containing all the flags to pass to

                          r.region command, it has to start with -
        """
        import grass.script.core as gcore
        if not flags.startswith('-'):
            raise Exception("flags parameter as to start with '-'")
        cmd = ['g.region', flags]
        out = self._run_command(cmd, ret=True)
        return gcore.parse_key_val(out)

    def db_describe(self, inp,  col=None):
        """Return information about vector table

        :param str inp: the name of input vector map to use as mask
        :param str col: the name of the column to get information
        """
        cmd = ['db.describe', '-c',  'table={inp}'.format(inp=inp)]
        out = self._run_command(cmd,  True)
        descr = {}
        for line in out.splitlines():
            if line.startswith('Column'):
                vals = line.split(':')
                key = vals[1].strip()
                type = vals[2].strip()
                leng = vals[3].strip()
                if not col:
                    descr[key] = {}
                    descr[key]['type'] = type
                    descr[key]['leng'] = leng
                elif col and col in line:
                    descr[key] = {}
                    descr[key]['type'] = type
                    descr[key]['leng'] = leng
                    return descr
        return descr

    def mask(self, inp=None, cats=None, vect=None, where=None,
             remove=False):
        """Set and remove the mask

        :param str inp: the name of input raster map to use as mask
        :param list cats:
        :param str vect: the name of input vector map to use as mask
        :param str where: the WHERE conditions of SQL statement without
                          'where' keyword

        :param bool remove: if True remove the mask
        """
        if not inp and not vect and not remove:
            raise Exception("For mask function you have to set or 'inp',"
                            " 'remove' or 'vector' parameter")
        elif inp:
            cmd = ['r.mask', 'raster={inp}'.format(inp=inp)]
            if cats:
                cmd.append('cats={inp}'.format(inp=','.join(cats)))
        elif vect:
            cmd = ['r.mask', 'vector={inp}'.format(inp=vect)]
            if where:
                cmd.append('where={st}'.format(st=where))
        elif remove:
            cmd = ['r.mask', '-r']
        self._run_command(cmd)

    def color(self, inp, rules="srtm"):
        """Set the color table for the given raster

        :param str inp: the input raster map, already imported in GRASS GIS
        :param str rules: the name of color table, see r.color manual for all

                          the existing table
        """
        cmd = ['r.colors', 'map={inm}'.format(inm=inp),
               'color={ru}'.format(ru=rules)]
        self._run_command(cmd)

    def copy(self, inp, out, typ="raster"):
        """Run g.copy to copy a map into new one

        :param str inp: the input raster map, already imported in GRASS GIS
        :param str out: the output raster map

        :param str typ: the type of map to copy
        """
        cmd = ['g.copy', "{ty}={i},{o}".format(ty=typ, i=inp, o=out)]
        self._run_command(cmd)

    def clump(self, inp, out):
        """Run r.clump to get values to replace with null

        :param str inp: the input raster map, already imported in GRASS GIS

        :param str out: the output raster map
        """
        cmd = ['r.clump', 'input={inm}'.format(inm=inp),
               'output={ou}'.format(ou=out)]
        self._run_command(cmd)

    def mapcal(self, expr):
        """Execute r.mapcalc


        :param str expr: a r.mapcalc expression
        """
        cmd = ['r.mapcalc', 'expression={ex}'.format(ex=expr)]
        self._run_command(cmd)

    def resamp(self, inp, out):
        """Execute downsampling if needed

        :param str inp: the input raster map, already imported in GRASS GIS

        :param str out: the output raster map
        """
        cmd = ['r.resamp.stats', '-w', 'input={inp}'.format(inp=inp),
               'output={ou}'.format(ou=out)]
        self._run_command(cmd)

    def interp(self, inp, out):
        """Execute interpolation if needed

        :param str inp: the input raster map, already imported in GRASS GIS

        :param str out: the output raster map
        """
        cmd = ['r.resamp.interp', 'input={inp}'.format(inp=inp),
               'output={ou}'.format(ou=out)]
        self._run_command(cmd)

    def patch(self, inps, out, typ="raster"):
        """Patch all the inputs in a out

        :param list inps: a list with the input maps

        :param str out: the name of output map
        """
        if typ == 'raster':
            cmd = ['r.patch']
        else:
            cmd = ['v.patch']
        cmd.append('input={ins}'.format(ins=','.join(inps)))
        cmd.append('output={ou}'.format(ou=out))
        self._run_command(cmd)

    def select(self, ainp, binp, out, ope='contains'):
        """Perform selection between two input using the operator

        :param str ainp: the name of the first input map
        :param str binp: the name of the second input map
        :param str out: the name of outut map

        :param str ope: the value of operator to use
        """
        cmd = ['v.select', 'ainput={inp}'.format(inp=ainp),
               'binput={inp}'.format(inp=binp), 'output={ou}'.format(ou=out),
               'operator={op}'.format(op=ope)]
        self._run_command(cmd)

    def buffer(self, inp, out, distance):
        """Create a buffer around input vector file

        :param str inp: the input raster map
        :param str out: the path to output raster map

        :param distance: the dimension of the buffer
        """
        cmd = ['v.buffer', 'input={inp}'.format(inp=inp),
               'output={ou}'.format(ou=out),
               'distance={dist}'.format(dist=distance)]
        self._run_command(cmd)

    def vinregion(self, out):
        """Create a vector map of the region boundary


        :param str out: the name of output vector
        """
        cmd = ['v.in.region', 'output={ou}'.format(ou=out)]
        self._run_command(cmd)

    def export(self, inp, out):
        """Function to export data from GRASS to GDAL format

        :param str inp: the input raster map

        :param str out: the path to output raster map
        """
        if self.config.get('output', 'format'):
            form = self.config.get('output', 'format')
        else:
            form = 'GTiff'
        out = "{na}.{fo}".format(na=out, fo=form.lower())
        cmd = ['r.out.gdal', 'input={inm}'.format(inm=inp),
               'output={ou}'.format(ou=out),
               'format={fo}'.format(fo=form)]
        self._run_command(cmd)

    def extract(self, inp, out, where):
        """Function to extract data from vector to new one

        :param str inp: the input raster map
        :param str out: the path to output raster map
        :param str where: the WHERE conditions of SQL statement without

                          'where' keyword
        """
        cmd = ['v.extract', 'input={inm}'.format(inm=inp),
               'output={ou}'.format(ou=out),
               'where={wh}'.format(wh=where)]
        self._run_command(cmd)

    def fillnull(self, inp, out, met):
        """Function to fill empty values.

        :param str inp: the input raster map
        :param str out: the path to output raster map

        :param str met: the method to use in
        """
        cmd = ['r.fillnulls', 'input={inm}'.format(inm=inp),
               'output={ou}'.format(ou=out),
               'method={me}'.format(me=met)]
        self._run_command(cmd)

    def random(self, out, num, mask):
        """Function to run v.random to create random point inside an
        area contained into mask vector

        :param str out: the name of output vector
        :param num: the number of vector points to create

        :param str mask: the name of vector map to use as mask
        """
        cmd = ['v.random', 'output={ou}'.format(ou=out),
               'npoints={nu}'.format(nu=str(num)),
               'restrict={re}'.format(re=mask)]
        self._run_command(cmd)

    def what_rast(self, inp, raster,  col='opendem'):
        """Query the raster map with a vector point data

        :param str inp: the input vector map

        :param str raster: the DTM raster map to query
        """
        cmd = ['v.db.addcolumn', 'map={inp}'.format(inp=inp),
               'columns="dem DOUBLE PRECISION"']
        self._run_command(cmd)

        cmd = ['v.what.rast', 'map={inp}'.format(inp=inp),
               'raster={rast}'.format(rast=raster),
               'column={inp}'.format(inp=col)]
        self._run_command(cmd)

    def univar(self,  inp,  col='opendem', perc=90):
        """Calculates univariate statistics of vector map
        and return the calculated values

        :param str inp: the name of input vector map
        :param str col: the name of the column to query
        :param perc: the value of percentile to calculate
        :return: univariate statistics values
        """
        import grass.script.core as gcore
        cmd = ['v.univar', '-ge' 'map={vec}'.format(vec=inp),
               'column={co}'.format(co=col),
               'percentile={per}'.format(per=perc)]
        out = self._run_command(cmd, ret=True)
        return gcore.parse_key_val(out)

    def dbselect_grouped(self,  inp,  col='opendem'):
        cmd = ["v.db.select" "-c" "map={vec}".format(vec=inp),
               "column={co}".format(co=col), "group={co}".format(co=col)]
        out = self._run_command(cmd, ret=True)
        return out

    def select_region_area(self, region):
        """Select """
        from grass.pygrass.vector import VectorTopo
        from grass.pygrass.vector.geometry import Point
        center = Point(0, 0)
        for c in self.get_region('-cg').splitlines():
            vals = c.split('=')
            if vals[0] == 'center_easting':
                center.x = vals[1]
            else:
                center.y = vals[1]
        vcenter = VectorTopo(self.centername)
        vcenter.open('w')
        vcenter.write(center)
        vcenter.close()
        self._select(self.name, self.centername, region)

    def cleanup(self, region, full=False):
        """Remove all the temporary file

        :param str region: the name of the region

        :param bool full: if set yes it remove completely the created mapset
        """
        if full:
            shutil.rmtree(self.mapsetpath, ignore_errors=True)
        #else:
            #TODO
