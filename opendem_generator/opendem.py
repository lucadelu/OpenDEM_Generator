#!/usr/bin/env python
from __future__ import print_function

import argparse
import textwrap

from .__init__ import __version__
from .download import DownloadData
from .process import ProcessData

DESCRIPTION = """OpenDEM Generator is a command line utility that makes it easy
to download, process and unify DTM dataset

        Commands:

                download:
                        opendem.py download path [-o] [-d] [-s]

                        required arguments:
                            path    Path to OpenDEM Generator configuration file

                        optional arguments:
                            -s --shapefile   Download shapefile for border
                            -o --outdir       Folder where store downloaded dataset
                            -d --debug      Activate debug

                process:

                        opendem.py process path [-o] [-d] [-s] [--no_overwrite]

                        required arguments:
                            path    Path to OpenDEM Generator configuration file

                        optional arguments:
                            -s --shapefile     Process shapefile for border
                            -o --outdir         Folder where downloaded dataset are stored
                            -d --debug        Activate debug
                            --no_overwrite  Disactivate overwrite in GRASS GIS


"""

def args_options():
    """ Generates an arugment parser.

    :returns: Parser object
    """
    # general parser
    parser = argparse.ArgumentParser(prog='opendem',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent(DESCRIPTION))
    parser.add_argument('--version', action='version',
                        version='%(prog)s version ' + __version__)
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Activate debug')
    parser.add_argument('-w', '--working_directory', dest='workdir',
                        help='Directory where data are stored')
    parser.add_argument('-s',  '--shapefile', action='store_true',
                        help='Download or process shapefile')
    parser.add_argument('confile', metavar='confile', nargs=1,
                        help="Path to OpenDEM Generator configuration file")
    # create subparsers for different command
    subparsers = parser.add_subparsers(help='OpenDEM Generator Utility',
                                       dest='subs')
    parser_down = subparsers.add_parser('download',
                                          help='Download needed data')
    parser_down.add_argument('-t','--test', action='store_true',
                             help='Tests urls for all the'\
                             ' regions in the configuration file')
    parser_proc = subparsers.add_parser('process',
                                        help='Download needed data')
    parser_proc.add_argument('--no_post_process', dest='nopost',
                             action='store_false',
                             help='Doesn\'t execute post processing')
    parser_proc.add_argument('--no_overwrite', action='store_false',
                             help='Disactivate overwrite in GRASS GIS',
                             dest='nover')

    return parser

def main(args):
    """Main function

    :param obj args: The Parser arguments
    :returns: 0 for success
    """
    if args:
        #debug
        debug = args.debug
        # working dir
        if args.workdir:
            out = args.workdir
        else:
            out = None
        # the sub command is download
        if args.subs == 'download':
            dd = DownloadData(args.confile, out, debug)
            if args.test:
                print(dd.test_urls())
                return 0
            if args.shapefile:
                dd.download_shapefile()
            dd.all_regions()
            return 0
        # the sub command is process
        elif args.subs == 'process':
            if args.nover:
                over = False
            else:
                over = True
            pd = ProcessData(args.confile, out, debug, over)
            if args.shapefile:
                pd.process_shapefile()
            pd.process_all_regions()
            if args.nopost:
                pd.post_process_all_regions()
            return 0

def __main__():
    parser = args_options()
    args = parser.parse_args()
    main(args)

if __name__ == "__main__":
    try:
        __main__()
    except (KeyboardInterrupt):
        exit('Received Ctrl + C... Exiting! Bye.', 1)
