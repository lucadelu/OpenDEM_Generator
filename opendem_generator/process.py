#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 15:50:38 2016

@author: lucadelu
"""
from __future__ import print_function
import logging
import ConfigParser
import os
import subprocess
import glob
from xml.sax.saxutils import escape
PIPE = subprocess.PIPE
from .utils import unzip, GdalFileInfo
try:
    import osgeo.gdal as gdal
    import osgeo.osr as osr
except ImportError:
    try:
        import gdal
        import osr
    except ImportError:
        raise ImportError('Python GDAL library not found, please install '
                          'python-gdal')
from .grassdem import GrassProcess


class ProcessData:
    """Class to process the data to create the final DTM"""

    def __init__(self, confile, out_dir=None, debug=False, overwrite=True):
        # configuration file
        self.config = ConfigParser.ConfigParser()
        self.config.read(confile)
        # useful info
        self.name = self.config.get('general', 'name').lower()
        self.regions = self.config.get('regions', 'list').split()
        self.resolution = self.config.get('general', 'resolution')
        # set destination folder
        if not out_dir:
            if self.config.get('general', 'out_dir'):
                out_dir = self.config.get('general', 'out_dir')
            else:
                raise Exception("Folder to store downloaded files has "
                                "to be set during initialization or in "
                                "the configure file into 'general' "
                                "section, 'out_dir' parameter")
        if os.access(out_dir, os.W_OK):
            self.out_dir = out_dir
        else:
            try:
                os.mkdir(out_dir)
                self.out_dir = out_dir
            except:
                raise Exception("Folder to store downloaded files does"
                                " not exist or is not writeable")
        # debug
        self.debug = debug
        # for logging
        log_filename = os.path.join(self.out_dir,
                                    '{pro}.log'.format(pro=self.name))
        log_format = '%(asctime)s - %(levelname)s - %(message)s'
        logging.basicConfig(filename=log_filename, level=logging.DEBUG,
                            format=log_format)
        # initialize empty variables
        self.shapefile = False
        self.shapename = None
        self.file_infos = {}
        # initialize GrassProcess
        self.grassp = GrassProcess(self.config)
        self.grassp.initialize_grass()

    def _names_to_fileinfos(self, names):
        """Translate a list of GDAL filenames, into file_info objects.
        Returns a list of file_info objects. There may be less file_info
        objects than names if some of the names could not be opened as GDAL
        files.
        """
        for k in names:
            fi = GdalFileInfo()
            if fi.init_from_name(k) == 1:
                self.file_infos[k] = fi

    def _create_gdal_vrt(self, output,  proj):
        """Write VRT file

        :param str output: the path and name of output file
        :param obj proj: an OSR object containing projection info
        """

        def _calculateNewSize(vals):
            """Return the new size of output raster

            :return: X size, Y size and geotransform parameters
            """
            l1 = vals[0]
            ulx = l1.ulx
            uly = l1.uly
            lrx = l1.lrx
            lry = l1.lry
            for fi in vals:
                ulx = min(ulx, fi.ulx)
                uly = max(uly, fi.uly)
                lrx = max(lrx, fi.lrx)
                lry = min(lry, fi.lry)
            psize_x = l1.geotransform[1]
            psize_y = l1.geotransform[5]

            geotransform = [ulx, psize_x, 0, uly, 0, psize_y]
            xsize = int((lrx - ulx) / geotransform[1] + 0.5)
            ysize = int((lry - uly) / geotransform[5] + 0.5)
            return xsize, ysize, geotransform

        def _calculateOffset(fileinfo, geotransform):
            """Return the offset between main origin and the origin of current
            file

            :param obj fileinfo: a file_info object
            :param obj geotransform: the geotransform parameters to keep x
                                     and y origin
            """
            x = abs(int((geotransform[0] - fileinfo.ulx) / geotransform[1]))
            y = abs(int((geotransform[3] - fileinfo.uly) / geotransform[5]))
            return x, y

        def write_complex(fil, geot):
            """Write a complex source to VRT file"""
            out.write('\t\t<ComplexSource>\n')
            out.write('\t\t\t<SourceFilename relativeToVRT="1">{name}'
                      '</SourceFilename>\n'.format(name=fil.filename.replace('"', '')))
            out.write('\t\t\t<SourceBand>1</SourceBand>\n')
            out.write('\t\t\t<SourceProperties RasterXSize="{x}" '
                      'RasterYSize="{y}" DataType="{typ}" '
                      'BlockXSize="{bx}" BlockYSize="{by}" />'
                      '\n'.format(x=fil.xsize, y=fil.ysize,
                                  typ=gdal.GetDataTypeName(v1.band_type),
                                  bx=fil.block_size[0], by=fil.block_size[1]))
            out.write('\t\t\t<SrcRect xOff="0" yOff="0" xSize="{x}" '
                      'ySize="{y}" />\n'.format(x=fil.xsize, y=fil.ysize))
            xoff, yoff = _calculateOffset(fil, geot)
            out.write('\t\t\t<DstRect xOff="{xoff}" yOff="{yoff}" '
                      'xSize="{x}" ySize="{y}" />'
                      '\n'.format(xoff=xoff, yoff=yoff, x=fil.xsize,
                                  y=fil.ysize))
            if fil.fill_value:
                out.write('\t\t\t<NODATA>{va}</NODATA>'
                          '\n'.format(va=fil.fill_value))
            out.write('\t\t</ComplexSource>\n')

        values = self.file_infos.values()
        xsize, ysize, geot = _calculateNewSize(values)
        v1 = values[0]
        out = open("{pref}".format(pref=output), 'w')
        out.write('<VRTDataset rasterXSize="{x}" rasterYSize="{y}">'
                  '\n'.format(x=xsize, y=ysize))
        if not v1.projection:
            out.write('\t<SRS>{proj}</SRS>\n'.format(proj=escape(proj.ExportToWkt(),
                                                                 entities={"'": "&apos;", "\"": "&quot;"})))
        else:
            out.write('\t<SRS>{proj}</SRS>\n'.format(proj=escape(v1.projection,
                                                                 entities={"'": "&apos;", "\"": "&quot;"})))
        out.write('\t<GeoTransform>{geo0}, {geo1}, {geo2}, {geo3},'
                  ' {geo4}, {geo5}</GeoTransform>\n'.format(geo0=geot[0],
                                                            geo1=geot[1],
                                                            geo2=geot[2],
                                                            geo3=geot[3],
                                                            geo4=geot[4],
                                                            geo5=geot[5]))
        out.write('\t<VRTRasterBand dataType="{typ}" band="1"'
                  '>\n'.format(typ=gdal.GetDataTypeName(v1.band_type)))
        if v1.fill_value:
            out.write('\t\t<NoDataValue>{va}</NoDataValue>'
                      '\n'.format(va=v1.fill_value))
        for v in values:
            write_complex(v, geot)
        out.write('\t</VRTRasterBand>\n')
        out.write('</VRTDataset>\n')
        out.close()

    def _all_regions_sufprefix(self, prefix=None, suffix=None):
        """Return all region name with a prefix or a suffix

        :param str prefix: a text to use as prefix
        :param str suffix: a text to use as suffix
        """
        output = []
        if prefix:
            form = "{add}_{name}"
            fix = prefix
        elif suffix:
            form = "{name}_{add}"
            fix = suffix
        else:
            raise Exception("You have to set prefix or suffix")
        for r in self.regions:
            output.append(form.format(add=fix, name=r))
        return output

    def _get_shepcolumn(self, region):
        """Return the column name to use to query vector shapefile

        :param str region: the name of inquiried region
        """
        shapecol = self.config.get(region, 'shapefile_column')
        if not shapecol:
            shapecol = self.config.get('general', 'shapefile_column')
        if not shapecol:
            raise Exception("The shapefile's column name was not "
                            "found. Please set it")
        return shapecol

    def set_shapefile(self):
        """This function has to be used to confirm that your shapefile
        is already imported and it set the right parameters to work
        correctly"""
        name = "{pre}_borders".format(pre=self.name)
        if self.grassp.exists(name, True):
            self.shapename = name
            self.shapefile = True
        else:
            print("Shapefile seems not to be imported")

    def process_shapefile(self):
        """This function will unzip and import the boundaries vector format
        into GRASS
        """
        ext = self.config.get('general', 'shapefile_format')
        zipp = False
        # get some information about shapefile
        if self.config.get('general', 'shapefile_zip') == 'true':
            zipp = True
        if zipp:
            name = "{inp}.zip".format(inp=self.name)
        else:
            name = "{inp}.{fo}".format(inp=self.name, fo=ext)
        infile = os.path.join(self.out_dir, name)
        if zipp:
            count = unzip(infile, self.out_dir, ext,  logging)
        if count == 0:
            raise Exception("No {fo} was found".format(fo=ext))
        shpname = self.config.get('general', 'shapefile_name')
        if shpname:
            infile = os.path.join(self.out_dir, shpname)
        self.shapename = "{pre}_borders".format(pre=self.name)
        # import the data
        self.grassp.import_vector(infile, self.shapename)
        self.shapefile = True

    def pre_process_single_region(self, region,  buffer_factor=-4):
        """This function is used to import data into GRASS
        This procedure is written by Markus Neteler into 2015 to merge
        different adjacent DEMs with aligned pixel positions and unique
        resolution

        :param str ragion: the name of region to process
        :param int buffer_factor: the value to calculate a buffer around
                                  the region of interest. It must be negative,
                                  values should be between -10 and -2
        """
        if buffer_factor >= 0:
            raise Exception("The buffer_factor value has to be negative")
        elif buffer_factor >= -2 or buffer_factor <= -10:
            print("The buffer_factor value should be between -10 and -2")
        # get info about the region
        indir = os.path.join(self.out_dir, region)
        files = glob.glob1(indir, '*')
        zipp = self.config.get(region, 'zip')
        ext = self.config.get(region, 'format')
        epsg = int(self.config.get(region, 'epsg'))
        res = self.config.get(region, 'resolution')
        # if collect more files should be present, otherwise only one
        if self.config.get(region, 'collect'):
            if len(files) <= 1:
                raise Exception("There should be more file for {re} region but"
                                " less then one was found".format(re=region))
            count = 0
            for fil in files:
                count += unzip(os.path.join(indir, fil), indir, ext, logging)
        else:
            if len(files) != 1:
                raise Exception("There should be only one file for {reg} region "
                                "but zero or more one was found".format(reg=region))
            if zipp == 'true':
                count = unzip(os.path.join(indir, files[0]), indir, ext,  logging)
            else:
                count = 1
        # get the name of files for this region
        files = [os.path.join(indir, f) for f in glob.glob1(indir, '*.{fo}'.format(fo=ext))]

        if not files:
            files = []
            for name in os.listdir(indir):
                newdir = os.path.join(indir, name)
                if os.path.isdir(newdir):
                    files.extend([os.path.join(newdir, f) for f in glob.glob1(newdir,
                                                                              '*.{fo}'.format(fo=ext))])
        # if only one file import or link it
        if len(files) == count and count == 1:
            self.grassp.add_to_grass(files[0], region, epsg)
        # otherwise create a vrt file and add it
        elif len(files) == count and count > 1:
            self._names_to_fileinfos(files)
            vrt = os.path.join(indir, "{reg}.gdalvrt".format(reg=region))
            proj = osr.SpatialReference()
            proj.ImportFromEPSG(epsg)
            self._create_gdal_vrt(vrt,  proj)
            self.grassp.add_to_grass(vrt, region, epsg)
        # otherwise return an error
        else:
            raise Exception("There should be at least one file for "
                            "{reg} region, but zero was found".format(reg=region))
        # set the region
        self.grassp.set_region(region)
        # if border shapefile exists use it to make borders
        regionmasked = "{re}_masked".format(re=region)
        if self.shapefile:
            shapecol = self._get_shepcolumn(region)
            shapeval = self.config.get(region, 'shapefile_value')
            dbdescr = self.grassp.db_describe(self.shapename, shapecol)
            if dbdescr[shapecol]['type'] == 'CHARACTER':
                shapewhere = "{col} = '{val}'".format(col=shapecol,
                                                      val=shapeval)
            else:
                shapewhere = "{col} = {val}".format(col=shapecol,
                                                      val=shapeval)
            self.grassp.mask(vect=self.shapename, where=shapewhere)
            mapcalcin = region
        # otherwise it get the region using the following procedure
        else:
            # clump it
            regionclump = '{suf}_clump'.format(suf=region)
            self.grassp.clump(region, regionclump)
            # vinregion
            regreg = '{suf}_region'.format(suf=region)
            self.grassp.vinregion(regreg)
            regbuf = '{suf}_buffer'.format(suf=region)
            self.grassp.buffer(regreg, regbuf, res * buffer_factor)
            regbuffinale = '{suf}_region_buffer'.format(suf=region)
            self.grassp.overlay(regreg, regbuf, regbuffinale, 'not')
            # vrandom
            regrandom = '{suf}_random'.format(suf=region)
            # number of points to create
            grassreg = self.grassp.get_region()
            # one point for pixel around region boundary
            # maybe we could moltiplicate it with buffer_factor value
            npoints = (grassreg['cols'] + grassreg['rows']) * 2
            self.grassp.random(regrandom, npoints, regbuffinale)
            # get values
            self.grassp.what_rast(regrandom, regionclump)
            # get univariate statistics
            univar = self.grassp.univar(regrandom)
            # get all values from random point
            distinct = self.grassp.dbselect_grouped(regrandom)
            # TODO how to choose values for rmapcalc if condition
            mapcalcin = region
        regionmasked = "{re}_masked".format(re=region)
        self.grassp.mapcal("{out} = {inp}".format(out=regionmasked,
                                                  inp=mapcalcin))
        self.grassp.mask(remove=True)
        ### TODO ad color and export in a if condition

    def process_single_region(self, region, color=None, export=False):
        """Process singolar region, depending the resolution of input
        raster and the output

        :param str ragion: the name of region to process
        :param str color: name of GRASS color table to set
        :param bool export: True to export data
        """
        regionmasked = "{re}_masked".format(re=region)
        regionresampl = "{re}_resampl".format(re=region)
        res = self.config.get(region, 'resolution')
        #
        if res < self.resolution:
            self.grassp.resamp(regionmasked, regionresampl)
        # interpolation
        elif res > self.resolution:
            self.grassp.interp(regionmasked, regionresampl)
        # just copy and doesn't export data
        elif res == self.resolution:
            self.grassp.copy(regionmasked, regionresampl)
            export = False
        # set the color table
        if color:
            self.grassp.color(regionresampl, color)
        # export data
        if export:
            out = os.path.join(self.out_dir,
                               "{na}_{res}".format(na=region,
                                                   res=self.resolution))
            self.grassp.export(regionresampl, out)

    def process_all_regions(self, color=False, export=False):
        """Entiry process data for all region

        :param str color: name of GRASS color table to set
        :param bool export: True to export data
        """
        # for each region import data into GRASS and preprocess
        for r in self.regions:
            self.pre_process_single_region(r)
        # set region to all maps
        newregions = self._all_regions_sufprefix(suffix="masked")
        self.grassp.set_region(inp=','.join(newregions), res=self.resolution)
        # for each region run process
        for r in self.regions:
            self.process_single_region(r, color, export)
        # patch the new raster in a bigger one
        newregions = self._all_regions_sufprefix(suffix="resampl")
        joined_output = "{na}_patched".format(na=self.name)
        self.grassp.patch(inps=newregions, out=joined_output)
        # execute the mapcalculation to bring the result map to float
        mapcalc_output = "{na}_mapcalc".format(na=self.name)
        self.grassp.mapcal("{out} = float({inp})".format(out=mapcalc_output,
                                                         inp=joined_output))
        print("{na} created".format(na=mapcalc_output))
        # set the color table
        if color:
            self.grassp.color(mapcalc_output, color)
        # export data
        if export:
            out = os.path.join(self.out_dir,
                               "{na}_{res}".format(na=self.name,
                                                   res=self.resolution))
            self.grassp.export(mapcalc_output, out)

    def post_process_all_regions(self, method="rst", color=False,
                                 export=False):
        """Cleanup the resulting DTM. Some problems could appear around
        the borders so we try to clean it. Shapefile is required

        :param str method: the method to use to fill no data
        :param str color: name of GRASS color table to set
        :param bool export: True to export data
        """
        buffers = []
        # for each region calculate a buffer around the boundaries
        for region in self.regions:
            shapecol = self._get_shepcolumn(region)
            shapeval = self.config.get(region, 'shapefile_value')
            shapewhere = "{col} = '{val}'".format(col=shapecol,
                                                  val=shapeval)
            self.grassp.extract(inp=self.shapename, out=region,
                          where=shapewhere)
            regionbuf = "{na}_buffer".format(na=region)
            bufferdistance = float(self.resolution) * 5
            self.grassp.buffer(inp=region, out=regionbuf,
                         distance=str(bufferdistance))
            buffers.append(regionbuf)
        # patch all the buffer together
        buf_patch = "{na}_buffer_patched".format(na=self.name)
        self.grassp.patch(inps=buffers, out=buf_patch, typ="vector")
        # set the mask to the new buffers
        self.grassp.mask(vect=buf_patch)
        # fillnull the no data pixel
        mapcalc_output = "{na}_mapcalc".format(na=self.name)
        #fillnull_output = "{na}_fill".format(na=self.name)
        final_ouput = "{na}_final".format(na=self.name)
        self.grassp.fillnull(inp=mapcalc_output, out=final_ouput, met=method)
        print("{na} created".format(na=final_ouput))
        # set the color table
        if color:
            self.grassp.color(final_ouput, color)
        # export data
        if export:
            out = os.path.join(self.out_dir,
                               "{na}_{res}_finale".format(na=self.name,
                                                          res=self.resolution))
            self.grassp.export(final_ouput, out)
